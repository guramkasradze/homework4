package com.example.homework4

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_posts_recyclerview_layout.view.*

class PostsRecyclerViewAdapter(private val posts:MutableList<UserModel.Data>, private val activity: MainActivity):RecyclerView.Adapter<PostsRecyclerViewAdapter.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_posts_recyclerview_layout, parent, false))
    }

    override fun getItemCount() = posts.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        private lateinit var model:UserModel.Data
        fun onBind() {
            model = posts[adapterPosition]
            Glide.with(activity).load(model.avatar).into(itemView.imageView)
            itemView.idTextView.text = model.id.toString()
            itemView.fullNameTextView.text = model.firstName + "  " + model.lastName
            itemView.mailTextView.text = model.email

            itemView.setOnClickListener() {
                activity.openProfile(model)
            }
        }
    }
}