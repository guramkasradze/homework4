package com.example.homework4

import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_posts_recyclerview_layout.view.*
import kotlinx.android.synthetic.main.profile_layout.view.*

class Profile(private val post:UserModel.Data, private val activity: MainActivity):RecyclerView.Adapter<Profile.ViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.profile_layout, parent, false))
    }

    override fun getItemCount() = 1

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.onBind()
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        fun onBind() {

            Glide.with(activity).load(post.avatar).into(itemView.profileImageView)
            itemView.profileIdTextView.text = post.id.toString()
            itemView.profileNameTextView.text = post.firstName + "  " + post.lastName
            itemView.profileMailTextView.text = post.email

            itemView.profileBackButton.setOnClickListener() {
                activity.openMain()
            }
        }
    }
}