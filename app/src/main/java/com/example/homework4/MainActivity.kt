package com.example.homework4

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log.d
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.gson.Gson
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {

    private lateinit var adapter:PostsRecyclerViewAdapter
    private lateinit var pAdapt:Profile

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        getUSers()
    }

    private fun getUSers() {

        DataLoader.getRequest("users", object: CustomCallback{
            override fun onSuccess(result: String) {
                val model = Gson().fromJson(result, UserModel::class.java)
                d("userCount", "${model.data.size}")

                init(model)
            }
        })

    }

    private fun init(model: UserModel) {
        recyclerView.layoutManager = LinearLayoutManager(this)
        adapter = PostsRecyclerViewAdapter(model.data, this)
        recyclerView.adapter = adapter
    }

    public fun openProfile(data: UserModel.Data) {


        recyclerView.layoutManager = LinearLayoutManager(this)
        pAdapt = Profile(data , this)
        recyclerView.adapter = pAdapt

    }

    public fun openMain() {
        recyclerView.adapter = adapter
    }
}
